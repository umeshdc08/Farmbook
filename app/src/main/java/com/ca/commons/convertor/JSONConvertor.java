package com.ca.commons.convertor;

import com.ca.commons.base.model.BaseModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collection;

public class JSONConvertor {
	
	private static GsonBuilder gsonBuilder = new GsonBuilder();
	
	public static String baseToJSON(Object object){
		
		String json = "";
		Gson gson = gsonBuilder.create();
		
		if(object!=null){
			json = gson.toJson(object);
		}
		return json;
	}
	
	public static Object JSONToObject(String JSONString, Class<? extends BaseModel> targetClass){
		
		Gson gson = gsonBuilder.create();
		return gson.fromJson(JSONString, targetClass);
	}
	
	// not tested, for later purpose
	public static Collection<? extends BaseModel> JSONToBaseList(String JSONString, Class<? extends BaseModel> targetClass){
		
		Type type = new TypeToken<Collection<? extends BaseModel>>(){}.getType();
		Gson gson = gsonBuilder.create();
		
		Collection<? extends BaseModel> JSONCollection = gson.fromJson(JSONString, type);
		return JSONCollection;
	}
}