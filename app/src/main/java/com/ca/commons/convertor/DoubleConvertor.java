package com.ca.commons.convertor;

import com.google.gson.*;

import java.lang.reflect.Type;

public class DoubleConvertor implements JsonSerializer<Double>, JsonDeserializer<Double>{

	@Override
	public Double deserialize(JsonElement jsonString, Type arg1,
			JsonDeserializationContext arg2) throws JsonParseException {
		Double returnValue = null;
		
		if(jsonString!=null && !jsonString.getAsString().trim().equals(""))
				returnValue = jsonString.getAsDouble();
		
		return returnValue;
	}

	@Override
	public JsonElement serialize(Double val, Type arg1,
			JsonSerializationContext arg2) {
		return new JsonPrimitive(val);
	}

}
