
package com.ca.commons.convertor;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConvertor implements JsonSerializer<Date>, JsonDeserializer<Date> {

	@Override
	public Date deserialize(JsonElement jsonString, Type arg1, JsonDeserializationContext arg2) throws JsonParseException {

		Date returnDate = null;
		if (jsonString != null) {
			if (jsonString.getAsString().contains(",")) { // assuming the format is mmm dd, yyyy
				SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
				try {
					returnDate = dateFormat.parse(jsonString.getAsString().toString());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else if (jsonString.getAsString().contains("T")) { // assuming thr format is dd-mm-yyyy
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				try {
					returnDate = dateFormat.parse(jsonString.getAsString().toString());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else if (jsonString.getAsString().contains("-")) { // assuming thr format is dd-mm-yyyy
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
				try {
					returnDate = dateFormat.parse(jsonString.getAsString().toString());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			} else {
				returnDate = new Date(jsonString.getAsLong());
			}
		}
		return returnDate;
	}

	@Override
	public JsonElement serialize(Date date, Type arg1, JsonSerializationContext arg2) {

		return new JsonPrimitive(date.getTime());
	}
}
