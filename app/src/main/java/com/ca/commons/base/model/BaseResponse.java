package com.ca.commons.base.model;

import java.io.Serializable;

public class BaseResponse implements Serializable{

	private static final long serialVersionUID = 1L;

	private String Status;
	
	private Integer StatusCode;

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public Integer getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(Integer statusCode) {
		StatusCode = statusCode;
	}

	public boolean isSuccessfulResponse(){
		if(StatusCode!= null && StatusCode == 0)
			return true;
		return false;
	}
	
}
