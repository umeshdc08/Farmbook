package com.ca.commons.base;

import com.ca.commons.controller.DataController;
import com.ca.commons.controller.NetworkController;

public class BaseController {
	
	private static NetworkController networkController;
	
	private static DataController dataController;
	
	public static NetworkController getNetworkController(){
		
		if(networkController == null)
			networkController = new NetworkController();
		
		return networkController; 
	}

	public static DataController getDataController(){
		
		if(dataController == null)
			dataController = new DataController();
		
		return dataController; 
	}
	
}
