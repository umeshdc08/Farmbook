package com.ca.commons.base;

import android.app.Fragment;
import com.ca.commons.IBase.CallBackInterface;

public class BaseFragment extends Fragment implements CallBackInterface {

	private String progressMessage = "Please wait";
	
	@Override
	public void asyncCallBackFunction(Object resultObject, String inputUrl) {
		System.out.println("call back in base activity");
	}

	public String getProgressMessage() {
		return progressMessage;
	}

	public void setProgressMessage(String progressMessage) {
		this.progressMessage = progressMessage;
	}
	
}
