package com.ca.commons.IBase;

public interface CallBackInterface {
	
	public void asyncCallBackFunction(Object resultObject, String givenUrl);

}
