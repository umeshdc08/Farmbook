package com.ca.commons.controller;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import com.ca.commons.base.BaseController;
import com.ca.commons.base.model.BaseModel;
import com.ca.commons.convertor.JSONConvertor;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class NetworkController extends BaseController {

	
	private HttpResponse makeRequest(String uri, String json) {
	    try {
	        HttpPost httpPost = new HttpPost(uri);
	        if(json!=null)
	        	httpPost.setEntity(new StringEntity(json));
	        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
	        httpPost.setHeader("Content-Type", "application/json");
	        httpPost.setHeader("Accept", "application/json");
	        return defaultHttpClient.execute(httpPost);
	    } catch (UnsupportedEncodingException e) {
	        e.printStackTrace();
	    } catch (ClientProtocolException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    return null;
	}
	
	
	// Given a URL, establishes an HttpUrlConnection and retrieves
	// the web page content as a InputStream, which it returns as
	// a string.
	public Object getDataForUrl(String url,Object inputModel, Class<? extends BaseModel> targetClass, boolean isCollection){
		String inputJSONString = "";
		String responseJSONString = "";
		Object returnClass = null;
		if(inputModel!=null) {
			inputJSONString = JSONConvertor.baseToJSON(inputModel);
			System.out.println(inputJSONString);
		}
		HttpResponse makeRequest = this.makeRequest(url, inputJSONString);
		
		if(makeRequest!=null){
			try {
				responseJSONString = EntityUtils.toString(makeRequest.getEntity());
				System.out.println(responseJSONString);
				if(responseJSONString!=null && responseJSONString.length()>0){
					if(targetClass == null)
						return responseJSONString;
					else if(isCollection)
						returnClass = JSONConvertor.JSONToBaseList(responseJSONString, targetClass);
					else
						returnClass = JSONConvertor.JSONToObject(responseJSONString, targetClass);
				}
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
		return returnClass;
		
	}
	
	public Bitmap getImageFromUrl(String Url){
		//HttpResponse response = this.makeRequest(Url, null);
		Bitmap myBitmap=null;
		if(Url!=null){
			try {
				//InputStream content = response.getEntity().getContent();
				URL urlConnection = new URL(Url);
				HttpURLConnection connection = (HttpURLConnection) urlConnection.openConnection();
				connection.setDoInput(true);
				connection.connect();
				InputStream input = connection.getInputStream();
				myBitmap = BitmapFactory.decodeStream(input);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return myBitmap;
		
	}
	
	public List<Bitmap> getImageListFromUrlList(List<String> urlList){
		List<Bitmap> imageList = new ArrayList<Bitmap>();
		if(urlList!=null && urlList.size()>0){
			for (String urlString : urlList) {
				Bitmap imageFromUrl = getImageFromUrl(urlString);
				if(imageFromUrl!=null)
					imageList.add(imageFromUrl);
			}
		}
		return imageList;
	}
	
}
	
