
package com.ca.commons.controller;

import android.app.Activity;
import com.ca.commons.base.BaseController;
import com.farmbook.model.Farmer;
import com.farmbook.model.Query;
import com.farmbook.utils.Global;

public class DataController extends BaseController {

    public static void doSignUp(String name, String username, String emailid, String phoneNo, String password, Activity activity){

        Farmer form = new Farmer();
        form.setName(name);
        form.setUsername(username);
        form.setPassword(password);
        form.setEmailId(emailid);
        form.setPhoneNo(phoneNo);
        form.setRequestType(Global.REQUEST_TYPE_SIGNUP);
        form.setDeviceId(Global.getSharedString(activity, Global.GCM_ID));

        new AsyncController(Global.FULL_URL, form, Farmer.class, activity);
    }

    public static void doSignIn(String emailid, String password, Activity activity){

        Farmer form = new Farmer();
        form.setUsername(emailid);
        form.setPassword(password);
        form.setRequestType(Global.REQUEST_TYPE_SIGNIN);
        form.setDeviceId(Global.getSharedString(activity, Global.GCM_ID));

        new AsyncController(Global.FULL_URL, form, Farmer.class, activity);
    }

    public static void doProfileUpdate(Farmer farmer, Activity activity){

        farmer.setRequestType(Global.PROFILE_UPDATE);
        farmer.setUsername(Global.getFarmerData(activity).getUsername());
        farmer.setPassword(Global.getFarmerData(activity).getPassword());
        new AsyncController(Global.FULL_URL, farmer, Farmer.class, activity);
    }

    public static void doPostQuestion(String queryStr, String imageString, String audioString, Activity activity){

        Query query = new Query();
        query.setUsername(Global.getFarmerData(activity).getUsername());
        query.setPassword(Global.getFarmerData(activity).getPassword());
        query.setQueryString(queryStr);
        query.setImageString(imageString);
        query.setAudioString(audioString);
        query.setRequestType(Global.REQUEST_TYPE_POST_QUESTION);
        new AsyncController(Global.FULL_URL, query, Farmer.class, activity);
    }

    public static void getAllQuestions(Farmer farmer,Activity activity){

        farmer.setRequestType(Global.REQUEST_TYPE_GET_ALL_QUESTION);
        new AsyncController(Global.FULL_URL, farmer, null, activity);
    }
}
