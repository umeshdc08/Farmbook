
package com.ca.commons.controller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import com.ca.commons.IBase.CallBackInterface;
import com.ca.commons.base.BaseController;
import com.ca.commons.base.model.BaseModel;
import com.ca.commons.base.model.BaseResponse;
import com.farmbook.R;

import java.util.concurrent.ExecutionException;

public class AsyncController extends AsyncTask<String, Void, Object> {

	private Class<? extends BaseModel>	targetClass;
	private Object						inputModel;
	private String url;
	private boolean						isCollection	= false;
	public Object						resultObject;
	private Activity context;
	private ProgressDialog				mProgressDialog;
	public boolean						taskCompleted	= false;

	public AsyncController(String url, Object inputModel, Class<? extends BaseModel> targetClass, boolean isCollection, Activity context) {

		this.url = url;
		this.targetClass = targetClass;
		this.inputModel = inputModel;
		this.isCollection = isCollection;
		if (context != null) {
            this.context = context;
			if(mProgressDialog==null){
				mProgressDialog = new ProgressDialog(context);
				mProgressDialog.setMessage(context.getResources().getString(R.string.pleaseWait));
				mProgressDialog.setCancelable(false);
                mProgressDialog.show();
			}
		}
		execute(url);
	}

	public AsyncController(String url, Object inputModel, Class<? extends BaseModel> targetClass, Activity context) {
		this(url, inputModel, targetClass, false, context);
	}

	@Override
	protected void onPreExecute() {
		taskCompleted = false;
		if (mProgressDialog != null)
			mProgressDialog.show();
	}

	@Override
	protected Object doInBackground(String... arg0) {
		NetworkController networkController = BaseController.getNetworkController();
		Object object = networkController.getDataForUrl(url, inputModel, targetClass, isCollection);
		return object;
	}

	@Override
	protected void onPostExecute(Object result) {

		resultObject = result;
		if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
		if (context != null)
			((CallBackInterface)context).asyncCallBackFunction(result, url);
	}

	@Deprecated
	public <T extends BaseModel> T getResultObject() {
		try {
			Object object = this.get();
			if (!(object instanceof BaseResponse))
				object = this.resultObject;
			else if ((object instanceof BaseModel)) {
				resultObject = object;
			} else
				resultObject = null;
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return (T) resultObject;
	}
}
