package com.farmbook.utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.farmbook.adapter.AdapterLanguage;
import com.farmbook.R;
import com.farmbook.activity.ActivitySignin;
import com.farmbook.activity.MainActivity;

import android.app.Dialog;

import java.util.Arrays;
import java.util.List;

public class LanguageDialog {

    Dialog dialog;
    List<String> languages;
    Activity activity;
    String fromActivity;

    public LanguageDialog(Activity activity, String fromActivity){

        this.activity = activity;
        languages = Arrays.asList(activity.getResources().getStringArray(R.array.languages));
        this.fromActivity = fromActivity;
    }

    public void dialogSelectLanguage() {

        LayoutInflater inflater;
        dialog = new Dialog(activity);
        inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        RelativeLayout layout = (RelativeLayout) inflater.inflate(R.layout.dialog_language_selection, null);
        ListView listView = (ListView) layout.findViewById(R.id.languageList);
        AdapterLanguage adapterLanguage = new AdapterLanguage(activity, languages);
        listView.setAdapter(adapterLanguage);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String lang = languages.get(position).substring(0,2).toLowerCase();
                Global.changeLang(activity, lang);
                dialog.dismiss();
                if(fromActivity.equals(ActivitySignin.class.getName().toString())){
                    Intent intent = new Intent(activity, ActivitySignin.class);
                    activity.startActivity(intent);
                    activity.finish();
                }else {
                    Intent intent = new Intent(activity, MainActivity.class);
                    activity.startActivity(intent);
                    activity.finish();
                }
            }
        });

        dialog = new android.app.Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layout);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }
}
