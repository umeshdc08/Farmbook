package com.farmbook.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import com.farmbook.R;
import com.farmbook.model.Crop;
import com.farmbook.model.Farmer;
import com.farmbook.model.FarmerQuery;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Global {

    public static String BASE_URL = "http://54.148.110.67:8080/FarmBook";
    //public static String BASE_URL = "http://192.168.0.106:8080";

    public static String FULL_URL = BASE_URL + "/FarmerController";
    public static String ENGLISH_CROP_RATE_URL = "http://raitamitra.kar.nic.in/ENG/index.asp";
    public static String KANNADA_CROP_RATE_URL = "http://raitamitra.kar.nic.in/kan/";

    public static String REQUEST_TYPE_SIGNUP = "signup";
    public static String REQUEST_TYPE_SIGNIN = "signin";
    public static String REQUEST_TYPE_UPDATE_PROFILE = "updateprofile";
    public static String REQUEST_TYPE_POST_QUESTION = "submitquery";
    public static String REQUEST_TYPE_GET_ALL_QUESTION = "getqueries";

    public static String PERFERENCE_NAME			= "farmbook_data";
    public static String FARMER_DATA				= "farmer_data";
    public static String PROFILE_UPDATE				= "updateprofile";
    public static String LANGUAGE				    = "Language";
    public static String QUESTION_ANSWER_STRING     = "query_answer_string";
    public static String GCM_ID                     = "gcm_id";

    public static int    ACTIVITY_RESULT_QUESTION_POSTED = 1;


    public static void setSharedString(Context context, String key, String s){

        SharedPreferences settings = context.getSharedPreferences(Global.PERFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, s);
        editor.commit();
    }

    public static String getSharedString(Context context, String key) {

        SharedPreferences settings = context.getSharedPreferences(Global.PERFERENCE_NAME, Context.MODE_PRIVATE);
        String s = settings.getString(key, "");
        System.out.println("Device Id: " + s);
        return s;
    }

    public static void setFarmerData(Context context, Farmer farmer) {

        String farmer_data = convertObjectToJson(farmer);
        SharedPreferences settings = context.getSharedPreferences(Global.PERFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Global.FARMER_DATA, farmer_data);
        editor.commit();
    }

    public static Farmer getFarmerData(Context context) {

        SharedPreferences settings = context.getSharedPreferences(Global.PERFERENCE_NAME, Context.MODE_PRIVATE);
        String format_data = settings.getString(Global.FARMER_DATA, null);
        System.out.println("get form data >>>>> " + format_data);
        try {
            Farmer farmer = (Farmer)convertJsonToObject(format_data, Farmer.class);
            return farmer;
        }catch (Exception e){
            return null;
        }
    }

    public static void saveLanguage(Context context, String language) {

        SharedPreferences settings = context.getSharedPreferences(Global.PERFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(Global.LANGUAGE, language);
        editor.commit();
    }

    public static String getLanguage(Context context) {

        SharedPreferences settings = context.getSharedPreferences(Global.PERFERENCE_NAME, Context.MODE_PRIVATE);
        String language = settings.getString(Global.LANGUAGE, null);
        return language;
    }

    public static void clearFarmerData(Context context) {

        SharedPreferences settings = context.getSharedPreferences(Global.PERFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        String farmat_data = "";
        editor.putString(Global.FARMER_DATA, farmat_data).commit();
    }

    public static String convertObjectToJson(Object object) {

        Gson gson = new Gson();
        String json = gson.toJson(object);
        return json;
    }

    public static Object convertJsonToObject(String json, Class<?> reqClass) {

        Gson gson = new Gson();
        Object obj = gson.fromJson(json, reqClass);
        return obj;
    }

    public static void displayToast(Context context, String s) {

        Toast toast = Toast.makeText(context, s, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 75);
        toast.show();
    }


    public static void loadLocale(Activity activity)
    {
        String language = getLanguage(activity);
        changeLang(activity, language);
    }

    public static void changeLang(Activity activity, String lang)
    {
        Locale myLocale;
        if (lang == null)
            return;
        myLocale = new Locale(lang);
        saveLanguage(activity, lang);
        Locale.setDefault(myLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = myLocale;
        activity.getBaseContext().getResources().updateConfiguration(config, activity.getBaseContext().getResources().getDisplayMetrics());
    }

    public static String imageString(Bitmap bitmap) {

        ByteArrayOutputStream bao = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bao);
        byte[] ba = bao.toByteArray();
        String ba1 = Base64.encodeToString(ba, Base64.DEFAULT);
        return ba1;
    }


    public static String fileToString(File file) {

        if(file!=null){


            FileInputStream fis = null;
            try {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                fis = new FileInputStream(file);

                byte[] buf = new byte[4096];
                int n;
                while (-1 != (n = fis.read(buf)))
                    baos.write(buf, 0, n);

                String fileAsStr = Base64.encodeToString(baos.toByteArray(), Base64.DEFAULT);
                System.out.println("file string "+fileAsStr);
                return  fileAsStr;
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    if(fis!=null)
                        fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            System.out.println("File cannot be null");
        }
        return "";
    }

        public static List<FarmerQuery> getFarmarQueries(Context context){

        String s = getSharedString(context, Global.QUESTION_ANSWER_STRING);
        if(s == null || s.equals(""))
            s = "[]";
        TypeToken<List<FarmerQuery>> typeToken = new TypeToken<List<FarmerQuery>>(){};
        Gson gson =  new Gson();
        List<FarmerQuery> farmerQueries = gson.fromJson(s, typeToken.getType());
        return farmerQueries;
    }

    public static void loadImage(final Context context, String url, ImageView view, final Drawable drawable, final boolean flag, boolean cacheInMemory) {

        DisplayImageOptions options = new DisplayImageOptions.Builder().showImageOnLoading(drawable).showImageForEmptyUri(drawable).showImageOnFail(drawable).cacheInMemory(cacheInMemory).cacheOnDisk(cacheInMemory).considerExifParams(true).bitmapConfig(Bitmap.Config.RGB_565).build();
        try {
            ImageLoader.getInstance().displayImage(url, view, options, new SimpleImageLoadingListener() {

                @Override
                public void onLoadingStarted(String imageUri, View view) {
                    if (!flag) {
                        ImageView imageView = (ImageView) view;
                        try {
                            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                            view.setBackground(new BitmapDrawable(context.getResources(), bitmap));
                            imageView.setImageResource(R.drawable.blank);
                            // imageView.setImageBitmap(bitmap);
                        } catch (NullPointerException e) {

                        } catch (ClassCastException e) {

                        }
                    }
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    if (!flag) {
                        ImageView imageView = (ImageView) view;
                        try {
                            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                            view.setBackground(new BitmapDrawable(context.getResources(), bitmap));
                            imageView.setImageResource(R.drawable.blank);
                            // imageView.setImageBitmap(bitmap);
                        } catch (ClassCastException e) {

                        } catch (NullPointerException e) {

                        }
                    }
                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    int a = 1;
                    // holder.progressBar.setVisibility(View.GONE);
                    if (!flag) {
                        ImageView imageView = (ImageView) view;
                        try {
                            BitmapDrawable ob = new BitmapDrawable(context.getResources(), loadedImage);
                            view.setBackground(ob);
                            imageView.setImageResource(R.drawable.blank);
                            // imageView.setImageBitmap(loadedImage);
                        } catch (Exception e) {

                        }
                    }
                }
            }, new ImageLoadingProgressListener() {

                @Override
                public void onProgressUpdate(String imageUri, View view, int current, int total) {

                }
            });
        } catch (Exception e) {

        }
    }

    public static String getCropRateUrl(Context context){

        String language = getLanguage(context);
        if(language.equalsIgnoreCase("ka")){
            return Global.KANNADA_CROP_RATE_URL;
        }else{
            return Global.ENGLISH_CROP_RATE_URL;
        }
    }

    public static List<Crop> cropList()
    {
        List<Crop> cropList = new ArrayList<>();
        Crop crop1=new Crop();
        Crop crop2=new Crop();

        crop1.setCropName("Wheat");
        crop1.setCropDesc("test crop1");

        crop2.setCropName("Paddy");
        crop2.setCropDesc("test crop2");

        cropList.add(crop1);
        cropList.add(crop2);

        return cropList;
    }
}
