package com.farmbook.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static final String MOBILE_PHONE_PATTERN = "\\d{10}";

    public static String isValidEmailId(String email){

        String error = null;
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        if(email == null || email.length() == 0)
            error = "Email id cannot be blank.";
        else if(!matcher.matches())
            error = "Please enter valid email id.";
        return error;
    }

    public static String isValidMobileNo(String phone){

        String error = null;
        if(phone == null || phone.length() == 0){

            error = "Please enter phone no";
        }
        Pattern pattern = Pattern.compile(MOBILE_PHONE_PATTERN);
        Matcher matcher = pattern.matcher(phone);
        if(!matcher.matches()){

            error = "Please enter valid phone no";
        }
        return error;
    }

    public static String isValidPassword(String password){

        String error = null;
        if(password == null || password.length() == 0){

            error = "Please enter password.";
        } else if (password.length()<6)
            error = "Password length should be at-least 6.";
        else if (password.length()>20){
            error = "Password length should not be more than 20.";
        }
        return error;
    }

    public static String isValidConfirmPassword(String password, String confirmPassword){

        String error = null;
        if(confirmPassword == null || confirmPassword.length() == 0)
            error = "Confirm password cannot be blank.";
        else if(!password.equals(confirmPassword))
            error = "Password mismatch.";
        return error;
    }

    public static String isValidName(String name, String fieldname){

        String error = null;
        if(name == null || name.length() == 0){

            error = "Please enter " + fieldname;
        }else if(name.length()<2){

            error = fieldname + " must be at-least of length 3";
        }
        return error;
    }

    public static String isNotBlank(String value, String fieldname){

        String error = null;
        if(value == null || value.equals("")){

            error = "Please enter " + fieldname;
        }
        return error;
    }
}