package com.farmbook.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.farmbook.R;
import com.farmbook.model.Crop;
import com.farmbook.utils.Global;

import java.util.List;

public class AdapterCrop extends BaseAdapter {

    List<Crop> crops;
    Activity activity;

    public AdapterCrop(Activity activity, List<Crop> crops){

        this.activity = activity;
        this.crops = crops;
    }

    @Override
    public int getCount() {
        return crops.size();
    }

    @Override
    public Object getItem(int position) {
        return crops.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView == null){
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.activity_crop, null);
            holder = new Holder();
            holder.cropName = (TextView) convertView.findViewById(R.id.cropname);
            holder.cropDesc = (TextView) convertView.findViewById(R.id.cropdesc);
            holder.cropImage = (ImageView) convertView.findViewById(R.id.cropImage);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.cropName.setText(activity.getResources().getString(activity.getResources().getIdentifier(crops.get(position).getCropName().toLowerCase() + "Name", "string", activity.getPackageName())));
        holder.cropDesc.setText(activity.getResources().getString(activity.getResources().getIdentifier(crops.get(position).getCropName().toLowerCase()+"Desc" , "string", activity.getPackageName())));
        holder.cropImage.setBackground(activity.getResources().getDrawable(activity.getResources().getIdentifier(crops.get(position).getCropName().toLowerCase() , "drawable", activity.getPackageName())));
        return convertView;
    }


    class Holder{

        TextView cropName, cropDesc;
        ImageView cropImage;
    }
}
