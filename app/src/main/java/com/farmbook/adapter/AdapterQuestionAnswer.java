package com.farmbook.adapter;

import android.app.Activity;
import android.media.AudioManager;
import android.media.Image;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.farmbook.R;
import com.farmbook.model.FarmerQuery;
import com.farmbook.model.QuestionAnswer;
import com.farmbook.utils.Global;

import java.util.List;

public class AdapterQuestionAnswer extends BaseAdapter {

    List<FarmerQuery> questionAnswers;
    Activity activity;

    public AdapterQuestionAnswer(Activity activity, List<FarmerQuery> questionAnswers){

        this.activity = activity;
        this.questionAnswers = questionAnswers;
    }

    @Override
    public int getCount() {
        return questionAnswers.size();
    }

    @Override
    public Object getItem(int position) {
        return questionAnswers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView == null){
            LayoutInflater mInflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.item_notification, null);
            holder = new Holder();
            holder.questionText = (TextView) convertView.findViewById(R.id.questionText);
            holder.answerText = (TextView) convertView.findViewById(R.id.answerText);

            holder.questionImage = (ImageView) convertView.findViewById(R.id.questionImage); holder.questionImage.setVisibility(View.GONE);
            holder.answerImage = (ImageView) convertView.findViewById(R.id.answerImage); holder.answerImage.setVisibility(View.GONE);

            holder.questionDate = (TextView) convertView.findViewById(R.id.questionTime);
            holder.answerDate = (TextView) convertView.findViewById(R.id.answerTime);

            holder.play_ans_sound = (LinearLayout) convertView.findViewById(R.id.play_answer_sound);
            holder.play_question_sound = (LinearLayout) convertView.findViewById(R.id.play_question_sound);

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        holder.questionText.setText(questionAnswers.get(position).getQueryString());
        holder.questionDate.setText(questionAnswers.get(position).getCreatedOn());

        String solution = questionAnswers.get(position).getSolution();
        String answeredOn = questionAnswers.get(position).getAnsweredOn();
        if(solution != null && !solution.equals(""))
            holder.answerText.setText(questionAnswers.get(position).getSolution());

        if(answeredOn != null && !answeredOn.equals(""))
            holder.answerDate.setText(questionAnswers.get(position).getAnsweredOn());

        if(questionAnswers.get(position).isqImageAvail()){

            holder.questionImage.setVisibility(View.VISIBLE);
            Global.loadImage(activity, formImageUrl(questionAnswers.get(position).getqImageUrl()), holder.questionImage, activity.getResources().getDrawable(R.drawable.background), false, true);
        }else{
            holder.questionImage.setVisibility(View.GONE);
        }

        if(questionAnswers.get(position).issImageAvail()){

            holder.answerImage.setVisibility(View.VISIBLE);
            Global.loadImage(activity, formImageUrl(questionAnswers.get(position).getsImageUrl()), holder.answerImage, activity.getResources().getDrawable(R.drawable.background), false, true);
        }else{
            holder.answerImage.setVisibility(View.GONE);
        }

        if(questionAnswers.get(position).issAudioAvail()){

            holder.play_ans_sound.setVisibility(View.VISIBLE);
            holder.play_ans_sound.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        MediaPlayer player = new MediaPlayer();
                        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        player.setDataSource(formSoundUrl(questionAnswers.get(position).getsAudioUrl()));
                        player.prepare();
                        player.start();

                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            });
        }else{

            holder.play_ans_sound.setVisibility(View.GONE);
        }

        if(questionAnswers.get(position).isqAudioAvail()){

            holder.play_question_sound.setVisibility(View.VISIBLE);
            holder.play_question_sound.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    try {
                        MediaPlayer player = new MediaPlayer();
                        player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                        player.setDataSource(formSoundUrl(questionAnswers.get(position).getqAudioUrl()));
                        player.prepare();
                        player.start();

                    } catch (Exception e) {
                        // TODO: handle exception
                    }
                }
            });
        }else{

            holder.play_question_sound.setVisibility(View.GONE);
        }

        return convertView;
    }

    private String formSoundUrl(String s){

        String url = Global.BASE_URL + s;
        //System.out.println("Image URL:" + url);
        return url;
        //return "http://54.148.110.67:8080/FarmBook/audio/solution46.mp3";
    }

    private String formImageUrl(String s){
        String url = Global.BASE_URL + s;
        System.out.println("Image URL:" + url);
        return url;
    }

    class Holder{

        TextView questionText, answerText, questionDate, answerDate;
        ImageView questionImage, answerImage;
        LinearLayout play_ans_sound, play_question_sound;
    }
}
