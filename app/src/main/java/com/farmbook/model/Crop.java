package com.farmbook.model;

public class Crop {

    String cropName,cropDesc,imageName;

    public String getCropDesc() {
        return cropDesc;
    }

    public String getCropName() {
        return cropName;
    }

    public void setCropDesc(String cropDesc) {
        this.cropDesc = cropDesc;
    }

    public void setCropName(String cropName) {
        this.cropName = cropName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
