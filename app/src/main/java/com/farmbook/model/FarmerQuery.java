package com.farmbook.model;

import com.ca.commons.base.model.BaseModel;

public class FarmerQuery extends BaseModel implements Comparable {

    Long id;
    String queryString;
    String imageString;
    String audioString;
    boolean qImageAvail;
    boolean qAudioAvail;
    Long farmerId;
    String solution;
    boolean sImageAvail;
    boolean sAudioAvail;
    Integer expertId;
    String createdOn;
    String answeredOn;
    Farmer farmer;
    String sImageUrl;
    String sAudioUrl;
    String qImageUrl;
    String qAudioUrl;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    public boolean isqImageAvail() {
        return qImageAvail;
    }

    public void setqImageAvail(boolean qImageAvail) {
        this.qImageAvail = qImageAvail;
    }

    public boolean isqAudioAvail() {
        return qAudioAvail;
    }

    public void setqAudioAvail(boolean qAudioAvail) {
        this.qAudioAvail = qAudioAvail;
    }

    public Long getFarmerId() {
        return farmerId;
    }

    public void setFarmerId(Long farmerId) {
        this.farmerId = farmerId;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public boolean issImageAvail() {
        return sImageAvail;
    }

    public void setsImageAvail(boolean sImageAvail) {
        this.sImageAvail = sImageAvail;
    }

    public boolean issAudioAvail() {
        return sAudioAvail;
    }

    public void setsAudioAvail(boolean sAudioAvail) {
        this.sAudioAvail = sAudioAvail;
    }

    public Integer getExpertId() {
        return expertId;
    }

    public void setExpertId(Integer expertId) {
        this.expertId = expertId;
    }

    public String getAudioString() {
        return audioString;
    }

    public void setAudioString(String audioString) {
        this.audioString = audioString;
    }

    public Farmer getFarmer() {
        return farmer;
    }

    public void setFarmer(Farmer farmer) {
        this.farmer = farmer;
    }

    public String getsImageUrl() {
        return sImageUrl;
    }

    public void setsImageUrl(String sImageUrl) {
        this.sImageUrl = sImageUrl;
    }

    public String getsAudioUrl() {
        return sAudioUrl;
    }

    public void setsAudioUrl(String sAudioUrl) {
        this.sAudioUrl = sAudioUrl;
    }

    public String getqImageUrl() {
        return qImageUrl;
    }

    public void setqImageUrl(String qImageUrl) {
        this.qImageUrl = qImageUrl;
    }

    public String getqAudioUrl() {
        return qAudioUrl;
    }

    public void setqAudioUrl(String qAudioUrl) {
        this.qAudioUrl = qAudioUrl;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getAnsweredOn() {
        return answeredOn;
    }

    public void setAnsweredOn(String answeredOn) {
        this.answeredOn = answeredOn;
    }

    @Override
    public int compareTo(Object another) {

        return createdOn.compareTo(((FarmerQuery)another).createdOn);
    }
}
