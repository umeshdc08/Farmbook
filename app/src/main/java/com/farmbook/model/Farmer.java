package com.farmbook.model;

import com.ca.commons.base.model.BaseModel;
import com.farmbook.utils.Global;

import java.io.Serializable;

public class    Farmer extends BaseModel implements Serializable {

    private Long id;

    private String name;

    private String emailId;

    private String phoneNo;

    private Integer age;

    private String gender;

    private String location;

    private String address;

    private String crops;

    private String equipment;

    private String deviceId;

    public Farmer() {

        name = "";
        emailId = "";
        phoneNo = "";
        age = 0;
        gender = "";
        address = "||||";
        crops = "";
        equipment = "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Address getAddress() {

        String address_data = this.address;
        try {
            Address address1 = (Address) Global.convertJsonToObject(address_data, Address.class);
            return address1;
        }catch (Exception e){
            return new Address();
        }
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getCrops() {
        return crops;
    }

    public void setCrops(String crops) {
        this.crops = crops;
    }

    public String getEquipment() {
        return equipment;
    }

    public void setEquipment(String equipment) {
        this.equipment = equipment;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
