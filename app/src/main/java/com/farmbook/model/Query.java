package com.farmbook.model;

import com.ca.commons.base.model.BaseModel;

public class Query extends BaseModel {

    String queryString;
    String imageString;
    String audioString;

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getImageString() {
        return imageString;
    }

    public void setImageString(String imageString) {
        this.imageString = imageString;
    }

    public String getAudioString() {
        return audioString;
    }

    public void setAudioString(String audioString) {
        this.audioString = audioString;
    }
}
