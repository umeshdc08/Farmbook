package com.farmbook.gcm;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Resources;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.farmbook.R;
import com.farmbook.activity.ActivitySignin;
import com.farmbook.activity.MainActivity;
import com.farmbook.activity.SplashScreen;
import com.farmbook.model.FarmerQuery;
import com.farmbook.utils.Global;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.List;


public class GcmMessageHandler extends IntentService {

    String title_String, body_String, imageUrl, schools, standards, divisions, type;
    Bundle extras;
    int id;
    private Handler handler;
    public GcmMessageHandler() {
        super("GcmMessageHandler");
    }

    @Override
    public void onCreate() {
        // TODO Auto-generated method stub
        super.onCreate();
        handler = new Handler();
    }
    @Override
    protected void onHandleIntent(Intent intent) {

        extras = intent.getExtras();

        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);
        title_String = "Testing";
        body_String = "Body String";

        String query = extras.get("query")!=null ? extras.get("query").toString() : "";
        System.out.println(">>>>>>>>> " + extras.toString());
        System.out.println("Query:" + query);

        TypeToken<FarmerQuery> typeToken = new TypeToken<FarmerQuery>(){};
        Gson gson =  new Gson();
        FarmerQuery farmerQueries = gson.fromJson(query, typeToken.getType());

        title_String = farmerQueries!=null? farmerQueries.getQueryString():null;
        body_String = farmerQueries!=null? farmerQueries.getSolution():null;

        showToast();
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    public void showToast(){
        handler.post(new Runnable() {
            public void run() {
                if (title_String != null || body_String != null)
                    showNotification();
            }
        });

    }

    public void showNotification() {

        if(Global.getFarmerData(this) != null){
            // Show notification only if user is logged in
            sendNotificatioon();
        }
    }

    private void sendNotificatioon(){

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, SplashScreen.class), 0);
        Resources r = getResources();

        Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Notification notification = new NotificationCompat.Builder(this)
                .setTicker("Ticker")
                .setSmallIcon(R.mipmap.ic_launcher_new)
                .setContentTitle(title_String)
                .setContentText(body_String)
                .setContentIntent(pi)
                .setAutoCancel(true)
                .setSound(soundUri)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }
}