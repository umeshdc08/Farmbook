package com.farmbook.gcm;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;

import com.farmbook.utils.Global;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

public class GCMManager {

    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";

    private static GoogleCloudMessaging gcm;
    private static String regid;
    private static String TAG = "SRApp-GCMManager";
    private static Context context;

    public static void subscribeForGCM(Context context, Context actContext) {
        //GCMManager.context = context;
        if (checkPlayServices(context)) {
            gcm = GoogleCloudMessaging.getInstance(context);
            regid = getRegistrationId(context);

            if (regid.isEmpty()) {
                Log.i(TAG, "Registering now");
                // registerInBackground(context, actContext);
            }
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
    }

    public static void unSubscribeForGCM(Context context, boolean showToast, Context actContext) {
        Log.i(TAG, "Un-registering GCM id");
        try {
            SharedPreferences sharedPref = context.getSharedPreferences(Global.PERFERENCE_NAME, Context.MODE_PRIVATE);
            String regID = sharedPref.getString(PROPERTY_REG_ID, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean checkPlayServices(Context context) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        if (resultCode != ConnectionResult.SUCCESS) {
            Log.i("SRApp-PlayServices", "This device is not supported.");
            return false;
        }
        return true;
    }

    private static String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the sign_up ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId.trim();
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {

            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private static SharedPreferences getGcmPreferences(Context context) {
        // This sample app persists the sign_up ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return context.getSharedPreferences(Global.PERFERENCE_NAME,
                Context.MODE_PRIVATE);
    }

    /*private static void registerInBackground(final Context context, final Context actContext) {
        new AsyncTask< Void, Void, String >() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    gcm.unregister();
                    regid = gcm.register(Global.GCMSRSignature);
                    Log.i(TAG, "Device registered, sign_up ID=" + regid);
                    // Persist the regID - no need to register again.
                    // storeRegistrationId(context, regid);

                    // You should send the sign_up ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                    // msg = sendRegistrationIdToBackend(context);
                    msg = regid;
                } catch (IOException e) {
                    Log.e(TAG, "Error :" + e.getMessage());
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.i(TAG, "onPostExecute" + msg);
                //if (msg != null && !msg.equals("")) {
                // Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                if(!getOneTimeCheckingAfterAppUpdate(context)) {
                    SetOneTimeCheckingAfterAppUpdate(context);
                }
            }
        }.execute(null, null, null);
    }

    public static boolean getOneTimeCheckingAfterAppUpdate(Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences(Global.PERFERENCE_NAME, Context.MODE_PRIVATE);
        return sharedPref.getBoolean(Global.ONE_TIME_AFTER_UPDATE, false);
    }

    public static void SetOneTimeCheckingAfterAppUpdate(Context context) {

        SharedPreferences sharedPref = context.getSharedPreferences(Global.PERFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = sharedPref.edit();
        ed.putBoolean(Global.ONE_TIME_AFTER_UPDATE, true);
        ed.commit();
    }*/

    public static String getRegid() {
        return regid;
    }
}