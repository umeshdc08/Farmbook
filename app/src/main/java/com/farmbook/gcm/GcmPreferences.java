package com.farmbook.gcm;

import android.app.Activity;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.farmbook.utils.Global;

import java.io.IOException;

public class GcmPreferences {

    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";

    static GoogleCloudMessaging gcm;
    static String PROJECT_NUMBER = "442238237758";
    static String regid;

    static String REG_ID = "";

    static String ERROR = "Error :SERVICE_NOT_AVAILABLE";

    public static void getRegId(final Activity activity){
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = Global.getSharedString(activity, Global.GCM_ID);
                if(msg.equals("")) {
                    try {
                        if (gcm == null) {
                            gcm = GoogleCloudMessaging.getInstance(activity.getApplicationContext());
                        }
                        gcm.unregister();
                        regid = gcm.register(PROJECT_NUMBER);

                        msg = regid;
                        Log.i("GCM", msg);
                        if (regid.equals(ERROR)) {
                            gcm = null;
                        }
                    } catch (IOException e) {
                        msg = "Error :" + e.getMessage();
                    }
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                REG_ID = msg;
                Global.setSharedString(activity, Global.GCM_ID, msg);
            }
        }.execute(null, null, null);
    }

    public static String getRegistrationId(Activity activity){

        REG_ID = Global.getSharedString(activity, Global.GCM_ID);
        if(REG_ID.equals(""))
            getRegId(activity);
        return REG_ID;
    }
}
