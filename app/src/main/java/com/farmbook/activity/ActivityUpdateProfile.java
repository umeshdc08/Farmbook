package com.farmbook.activity;

import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioGroup;

import com.ca.commons.IBase.CallBackInterface;
import com.ca.commons.controller.DataController;
import com.farmbook.R;
import com.farmbook.model.Address;
import com.farmbook.model.Farmer;
import com.farmbook.utils.Global;
import com.rengwuxian.materialedittext.MaterialEditText;

public class ActivityUpdateProfile extends AppCompatActivity implements CallBackInterface {

    MaterialEditText name, username, email, phone;
    MaterialEditText age, address1, address2, state, city, pincode;
    RadioGroup gender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_update_profile);

        setupToolbar();
        setupViews();
    }

    private void setupViews(){

        name = (MaterialEditText) findViewById(R.id.name);
        username = (MaterialEditText) findViewById(R.id.username);
        email = (MaterialEditText) findViewById(R.id.email);
        phone = (MaterialEditText) findViewById(R.id.phone);
        age = (MaterialEditText) findViewById(R.id.age);
        address1 = (MaterialEditText) findViewById(R.id.addressline1);
        address2 = (MaterialEditText) findViewById(R.id.addressline2);
        state = (MaterialEditText) findViewById(R.id.state);
        city = (MaterialEditText) findViewById(R.id.city);
        pincode = (MaterialEditText) findViewById(R.id.pincode);
        gender = (RadioGroup) findViewById(R.id.gender);
        prePopulateValues();
    }

    private void prePopulateValues(){

        Farmer farmer = Global.getFarmerData(this);
        name.setText(farmer.getName()); name.setEnabled(false);
        username.setText(farmer.getUsername()); username.setEnabled(false);
        email.setText(farmer.getEmailId()); email.setEnabled(false);
        phone.setText(farmer.getPhoneNo());
        age.setText(farmer.getAge().toString());
        Address address3 = farmer.getAddress();
        address1.setText(address3.getAddress1());
        address2.setText(address3.getAddress2());
        state.setText(address3.getState());
        city.setText(address3.getCity());
        pincode.setText(address3.getPincode());

        if(farmer.getGender().equalsIgnoreCase("male")){

            gender.check(R.id.gender_male);
        } else {

            gender.check(R.id.gender_female);
        }
    }

    private void setupToolbar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Update profile");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.update_profile_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_save) {
            if(validateForm()){
                saveProfile();
                return true;
            } else
                return false;

        }
        return super.onOptionsItemSelected(item);
    }

    private boolean validateForm() {
        String ageString = this.age.getText().toString();
        if(!ageString.isEmpty()){
            int ageInt = Integer.parseInt(ageString);
            if(ageInt>6 && ageInt<90)
                return true;
            else
                Global.displayToast(this,"Age should be between 6 to 90 years");
        }
        return false;
    }

    Farmer farmer;
    private void saveProfile(){

        Address address = new Address();
        address.setAddress1(address1.getText().toString());
        address.setAddress2(address2.getText().toString());
        address.setState(state.getText().toString());
        address.setCity(city.getText().toString());
        address.setPincode(pincode.getText().toString());

        farmer = new Farmer();
        farmer.setName(name.getText().toString());
        farmer.setUsername(username.getText().toString());
        farmer.setEmailId(email.getText().toString());
        farmer.setPhoneNo(phone.getText().toString());
        farmer.setAge(Integer.parseInt(age.getText().toString()));
        farmer.setAddress(Global.convertObjectToJson(address));

        String gen = "male";
        switch (gender.getCheckedRadioButtonId()){
            case R.id.gender_male:
                gen = "male";
                break;
            case R.id.gender_female:
                gen = "female";
                break;
        }
        farmer.setGender(gen);

        DataController.doProfileUpdate(farmer, this);
    }

    @Override
    public void asyncCallBackFunction(Object resultObject, String inputUrl) {

        if(inputUrl.equals(Global.FULL_URL)){
            if(resultObject!=null){

                try{
                    System.out.println("Update profile :" + resultObject);
                    Global.setFarmerData(ActivityUpdateProfile.this, (Farmer) resultObject);
                    Global.displayToast(this, "Profile updated successfully");
                    this.finish();
                }catch (Exception e){
                    Global.displayToast(ActivityUpdateProfile.this, ActivityUpdateProfile.this.getResources().getString(R.string.someTingWentWrong));
                }
            }
        }
    }
}