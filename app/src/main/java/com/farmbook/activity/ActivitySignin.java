package com.farmbook.activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.ca.commons.IBase.CallBackInterface;
import com.ca.commons.controller.DataController;
import com.farmbook.R;
import com.farmbook.model.Farmer;
import com.farmbook.utils.Global;
import com.farmbook.utils.LanguageDialog;
import com.farmbook.utils.Validation;
import com.gc.materialdesign.views.ButtonFlat;
import com.gc.materialdesign.views.ButtonRectangle;

public class ActivitySignin extends AppCompatActivity implements CallBackInterface {

    ButtonFlat newUser;
    ButtonRectangle signin;
    EditText username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        setupAllViews();
    }

    private void setupAllViews(){

        setupUsername();
        setupPassword();
      //  setupForgotPassword();
        setupNewUser();
        setupSignin();

        if(Global.getLanguage(this) == null)
            new LanguageDialog(this, ActivitySignin.class.getName()).dialogSelectLanguage();
    }

    private void setupUsername(){

        username = (EditText) findViewById(R.id.username);
    }

    private void setupPassword(){

        password = (EditText) findViewById(R.id.password);
    }

    private void setupSignin(){

        signin = (ButtonRectangle) findViewById(R.id.signin);
        signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                doSignin();
            }
        });
    }

    /*private void setupForgotPassword(){

        forgotPassword = (ButtonFlat) findViewById(R.id.forgotPassword);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }*/

    private void doSignin(){

        String _username = username.getText().toString();
        String _password = password.getText().toString();
        if(validateData(_username, _password)){

            signup(_username, _password);
        }
    }

    private boolean validateData(String username, String password){

        boolean is_valid = true;
        String email_error = Validation.isNotBlank(username, "Username");
        String password_error = Validation.isNotBlank(password, "password");
        if(email_error != null){

            is_valid = false;
            Global.displayToast(getBaseContext(), email_error);
        }else if(password_error != null){

            is_valid = false;
            Global.displayToast(getBaseContext(), password_error);
        }
        return is_valid;
    }

    private void setupNewUser(){

        newUser = (ButtonFlat) findViewById(R.id.newuser);
        newUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                gotoSignup();
            }
        });
    }

    private void gotoSignup(){

        Intent intent = new Intent(this, ActivitySignup.class);
        startActivity(intent);
    }

    private void gotoHomScreen(){

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void signup(String emailid, String password){

        DataController.doSignIn(emailid, password, this);
    }

    private void saveSignupData(Farmer farmer){

        Global.setFarmerData(ActivitySignin.this, farmer);
    }

    @Override
    public void asyncCallBackFunction(Object resultObject, String inputUrl) {

        if(inputUrl.equals(Global.FULL_URL)){
            if(resultObject!=null){

                try{
                    Farmer farmer = (Farmer) resultObject;
                    if(farmer.getId()!=null){
                        saveSignupData(farmer);
                        gotoHomScreen();
                    } else {
                        Global.displayToast(getBaseContext(), "Invalid username/password");
                    }

                }catch (Exception e){
                    Global.displayToast(getBaseContext(), "Something went wrong!! Please try sometime later.");
                }

            } else {
                Global.displayToast(getBaseContext(), "Invalid username/password");
            }
        }
    }
}
