package com.farmbook.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.ca.commons.IBase.CallBackInterface;
import com.ca.commons.controller.DataController;
import com.farmbook.R;
import com.farmbook.fragments.FragmentCropList;
import com.farmbook.fragments.FragmentTimeline;
import com.farmbook.model.Farmer;
import com.farmbook.utils.Global;
import com.farmbook.utils.LanguageDialog;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, CallBackInterface {

    TextView username, emailid;
    Farmer farmer;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        farmer = Global.getFarmerData(this);
        setupAllViews();
    }

    private void setupAllViews(){

        setupNavigationDrawer();
        setupNavigationHeader();
        DataController.getAllQuestions(Global.getFarmerData(this), this);
    }

    private void setupNavigationHeader(){

        View headerLayout = navigationView.getHeaderView(0);
        username = (TextView) headerLayout.findViewById(R.id.nav_username1);
        emailid = (TextView) headerLayout.findViewById(R.id.nav_emailid1);
        username.setText(farmer.getUsername());
        emailid.setText(farmer.getEmailId());
        headerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                go_to_update_profile();
            }
        });
    }

    private void go_to_update_profile(){

        Intent intent = new Intent(MainActivity.this, ActivityUpdateProfile.class);
        startActivity(intent);
        drawer.closeDrawers();
    }

    DrawerLayout drawer;
    private void setupNavigationDrawer(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        // loadTimelineFragment();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        /*if (id == R.id.nav_contact_us) {

            loadTimelineFragment();
        } else*/
        if (id == R.id.nav_logout) {

            logoutDialog();
        } else if (id == R.id.nav_change_language){

            new LanguageDialog(this, MainActivity.class.getName()).dialogSelectLanguage();
        } else if (id == R.id.nav_crop_rates){

            goToCropRatesActivity();
        } else if (id == R.id.nav_edit_profile){

            go_to_update_profile();
        } else if(id == R.id.nav_crop){
           // item.setVisible(false);
            if(this.getResources().getString(R.string.cropDetail).equals(item.getTitle())){
                item.setTitle(R.string.timeline);
                loadFragmentCropList();
            } else {
                item.setTitle(R.string.cropDetail);
                loadTimelineFragment();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void goToCropRatesActivity(){

        Intent intent = new Intent(this, ActivityCropRates.class);
        startActivity(intent);
    }

    private void logoutDialog(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to logout");
        builder.setTitle("Logout");
        builder.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                Global.clearFarmerData(MainActivity.this);
                gotoSignin();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void gotoSignin(){

        Intent intent = new Intent(MainActivity.this, ActivitySignin.class);
        startActivity(intent);
        finish();
    }

    public void loadTimelineFragment(){

        FragmentTimeline fragmentTimeline = new FragmentTimeline();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragmentTimeline);
        fragmentTransaction.commit();
        setTitle(getResources().getString(R.string.app_name));
    }

    public void loadFragmentCropList(){

        FragmentCropList fragmentcrop = new FragmentCropList();
        android.support.v4.app.FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragmentcrop);
        fragmentTransaction.commit();
        setTitle(getResources().getString(R.string.crop_list_view));
    }

    @Override
    public void asyncCallBackFunction(Object resultObject, String inputUrl) {

        if (inputUrl.equals(Global.FULL_URL)) {
            if (resultObject != null) {
                try {
                    System.out.print(">>>>> " + resultObject);
                    Global.setSharedString(MainActivity.this, Global.QUESTION_ANSWER_STRING, (String) resultObject);
                    loadTimelineFragment();
                } catch (Exception e) {
                    Global.displayToast(getBaseContext(), "Something went wrong!! Please try sometime later.");
                }
            }
        } else {
            Global.displayToast(getBaseContext(), "Invalid username/password");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Global.ACTIVITY_RESULT_QUESTION_POSTED){

            DataController.getAllQuestions(Global.getFarmerData(this), this);
        }
    }
}