package com.farmbook.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.farmbook.R;
import com.farmbook.gcm.GcmPreferences;
import com.farmbook.utils.Global;

public class SplashScreen extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        initialize();
        Global.loadLocale(this);
        setContentView(R.layout.activity_flash);
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {

                // This method will be executed once the timer is over
                // Start your app main activity
                if(Global.getFarmerData(SplashScreen.this) != null){
                    Intent i = new Intent(SplashScreen.this, MainActivity.class);
                    startActivity(i);
                }else{
                    Intent i = new Intent(SplashScreen.this, ActivitySignin.class);
                    startActivity(i);
                }

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private void initialize(){

        if(Global.getSharedString(SplashScreen.this, Global.GCM_ID).equals("")){

            GcmPreferences.getRegId(SplashScreen.this);
        }
        // GcmPreferences.getRegistrationId(ActivityWelcomeScreen.this);
        // Fabric.with(this, new Crashlytics());
    }
}
