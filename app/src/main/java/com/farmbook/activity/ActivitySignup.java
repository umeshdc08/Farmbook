package com.farmbook.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.ca.commons.IBase.CallBackInterface;
import com.ca.commons.controller.DataController;
import com.farmbook.R;
import com.farmbook.model.Farmer;
import com.farmbook.utils.Global;
import com.farmbook.utils.Validation;
import com.gc.materialdesign.views.ButtonFlat;
import com.gc.materialdesign.views.ButtonRectangle;
import com.rengwuxian.materialedittext.MaterialEditText;

public class ActivitySignup extends AppCompatActivity implements CallBackInterface {

    MaterialEditText name, username, password,  confirmPassword, email, phone;
    ButtonFlat existingUser;
    ButtonRectangle signup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        setupAllViews();
    }

    private void setupAllViews(){

        setupAllEditText();
        setupSignup();
        setupExistingUser();
    }

    private void setupAllEditText(){

        name = (MaterialEditText) findViewById(R.id.name);
        username = (MaterialEditText) findViewById(R.id.username);
        password = (MaterialEditText) findViewById(R.id.password);
        confirmPassword = (MaterialEditText) findViewById(R.id.confirmPassword);
        email = (MaterialEditText) findViewById(R.id.email);
        phone = (MaterialEditText) findViewById(R.id.phone);
    }

    private void setupSignup(){

        signup = (ButtonRectangle) findViewById(R.id.signup);
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(isValidData()){
//                    displayToast("Welcome to foembook");
//                    gotoHome();
//                }
                validateDataAndSignup();
            }
        });
    }

    private boolean validateDataAndSignup(){

        String _name = name.getText().toString();
        String _username = username.getText().toString();
        String _password = password.getText().toString();
        String _confirmPassword = confirmPassword.getText().toString();
        String _email = email.getText().toString();
        String _phone = phone.getText().toString();
        boolean is_valid = true;

        String name_error = Validation.isValidName(_name, "Name");
        String username_error = Validation.isValidName(_username, "Username");
        String password_error = Validation.isValidPassword(_password);
        String confirmPwd_error = Validation.isValidConfirmPassword(_password, _confirmPassword);
        String email_error = Validation.isValidEmailId(_email);
        String phone_error = Validation.isValidMobileNo(_phone);

        if(name_error != null){
            Global.displayToast(this, name_error);
            is_valid = false;
            name.requestFocus();
        }else if (username_error != null){
            Global.displayToast(this, username_error);
            is_valid = false;
            username.requestFocus();
        }else if (password_error != null){
            Global.displayToast(this, password_error);
            is_valid = false;
            password.requestFocus();
        }
        else if (confirmPwd_error != null){
            Global.displayToast(this, confirmPwd_error);
            is_valid = false;
            confirmPassword.requestFocus();
        }else if (phone_error != null) {
            Global.displayToast(this, email_error);
            is_valid = false;
            phone.requestFocus();
        }else if (email_error != null) {
            Global.displayToast(this, email_error);
            is_valid = false;
            email.requestFocus();
        }
        if(is_valid){
            doSignup(_name, _username, _email, _phone, _password);
        }else{
            Global.displayToast(this, "Sorry!! Something bad in input data");
        }

        return is_valid;
    }

    private void setupExistingUser(){

        existingUser = (ButtonFlat) findViewById(R.id.existingUser);
        existingUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                goToSignin();
            }
        });
    }

    private void goToSignin(){

        finish();
    }

    private void gotoHome(){

        Intent intent = new Intent(ActivitySignup.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void doSignup(String name, String username, String emailid, String phoneNo, String password){

        DataController.doSignUp(name, username, emailid, phoneNo, password, this);
    }

    private void saveSignupData(Farmer farmer){

        Global.setFarmerData(ActivitySignup.this, farmer);
    }

    @Override
    public void asyncCallBackFunction(Object resultObject, String inputUrl) {

        if(inputUrl.equals(Global.FULL_URL)){
            if(resultObject!=null){

                try{
                    Farmer farmer = (Farmer) resultObject;
                    saveSignupData(farmer);
                    gotoHome();
                }catch (Exception e){
                    Global.displayToast(this, "Something went wrong!! Please try sometime later.");
                }
            }
        }
    }
}
