package com.farmbook.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.farmbook.R;
import com.farmbook.utils.Global;

/**
 *
 */
public class ActivityCropRates extends AppCompatActivity {

    WebView cropRatesWebView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_crop_reates);
        setupToolbar();
        setupViews();
    }

    private void setupToolbar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(this.getResources().getString(R.string.cropRates));
    }

    private void setupViews(){

        setupWebView();
    }

    private void setupWebView(){

        cropRatesWebView = (WebView) findViewById(R.id.webView);
        cropRatesWebView.setWebViewClient(new WebViewClient());
        cropRatesWebView.loadUrl(Global.getCropRateUrl(this));
    }
}