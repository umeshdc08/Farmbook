package com.farmbook.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ca.commons.IBase.CallBackInterface;
import com.ca.commons.controller.DataController;
import com.farmbook.R;
import com.farmbook.utils.Global;
import com.gc.materialdesign.views.ButtonRectangle;

import org.w3c.dom.Text;

import java.io.File;
import java.io.IOException;

public class ActivityQuestionForm extends AppCompatActivity implements CallBackInterface {

    EditText question;
    FloatingActionButton attachImage, recordQuestion, playRecording, deleteRecording, reRecording;
    Activity activity;
    CardView recordClip;
    TextView recordDuration;

    final int HOUR = 60*60*1000;
    final int MINUTE = 60*1000;
    final int SECOND = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_form);
        activity = this;

        setupToolbar();
        setupViews();
    }

    private void setupViews(){

        setupQuestionEditTextView();
        setupAttachImageButton();
        setupRecordButton();
        setUpImage();
        setupRecordClip();
    }

    public void setUpImage(){

        attachmentImage = (ImageView) findViewById(R.id.image);
    }

    private void setupQuestionEditTextView(){

        question = (EditText) findViewById(R.id.question);
    }

    private void setupAttachImageButton(){

        attachImage = (FloatingActionButton) findViewById(R.id.attachImage);
        attachImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                openImageSelection();
            }
        });
    }

    private void setupRecordClip(){

        recordClip = (CardView) findViewById(R.id.recordClip);
        recordDuration = (TextView) findViewById(R.id.recordDuration);
        setUpPlayRecording();
        setUpDeleteRecording();
        setUpReRecording();
        recordClip.setVisibility(View.GONE);
    }

    private void showRecordClip(){

        recordQuestion.setVisibility(View.GONE);
        recordClip.setVisibility(View.VISIBLE);
        setRecordDuration();
    }

    private void deleteRecordClip(){

        recordQuestion.setVisibility(View.VISIBLE);
        recordClip.setVisibility(View.GONE);
    }

    private void setRecordDuration(){

        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(audioFilePath);
            int durationInMillis = mediaPlayer.getDuration();

            int durationHour = durationInMillis/HOUR;
            int durationMint = (durationInMillis%HOUR)/MINUTE;
            int durationSec = (durationInMillis%MINUTE)/SECOND;

            int seconds = durationInMillis / 1000;


//            String s = String.format("%2d", durationHour) + ":" +
//                    String.format("%2d", durationMint) + ":" + String.format("%2d", durationSec);
            recordDuration.setText("" + seconds);
        }catch (IOException e){

        }
    }

    int RESULT_LOAD_IMAGE = 5;
    Bitmap bitmap = null;
    ImageView attachmentImage;
    public void openImageSelection(){

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data){

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            // final BitmapFactory.Options options = new BitmapFactory.Options();
            // options.inSampleSize = 8;
            // options.inJustDecodeBounds = true;
            // bitmap = BitmapFactory.decodeFile(picturePath, options);
            bitmap = makeBitmap(picturePath);
            attachmentImage.setImageBitmap(bitmap);
//            if (bitmap!=null){
//                bitmap.recycle();
//                bitmap = null;
//            }
        }
    }

    private Bitmap makeBitmap(String path) {

        try {
            final int IMAGE_MAX_SIZE = 1200000; // 1.2MB
            //resource = getResources();

            // Decode image size
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(path, options);

            int scale = 1;
            while ((options.outWidth * options.outHeight) * (1 / Math.pow(scale, 2)) >
                    IMAGE_MAX_SIZE) {
                scale++;
            }

            Bitmap pic = null;
            if (scale > 1) {
                scale--;
                // scale to max possible inSampleSize that still yields an image
                // larger than target
                options = new BitmapFactory.Options();
                options.inSampleSize = scale;
                pic = BitmapFactory.decodeFile(path, options);

                // resize to desired dimensions

                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.y;
                int height = size.x;

                //int height = imageView.getHeight();
                //int width = imageView.getWidth();

                double y = Math.sqrt(IMAGE_MAX_SIZE
                        / (((double) width) / height));
                double x = (y / height) * width;

                Bitmap scaledBitmap = Bitmap.createScaledBitmap(pic, (int) x, (int) y, true);
                pic.recycle();
                pic = scaledBitmap;

                System.gc();
            } else {
                pic = BitmapFactory.decodeFile(path);
            }

            return pic;

        } catch (Exception e) {
            return null;
        }

    }

    private void setupRecordButton(){

        recordQuestion = (FloatingActionButton) findViewById(R.id.record);
        recordQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            dialogRecording();
            }
        });
    }

    private void setupToolbar(){

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Post your Question?");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.post_question_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_post_question) {
            postQuestion();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void postQuestion(){

        String query = question.getText().toString();
        String imageString = "";
        String soundString = null;
        if(bitmap!=null){
            imageString = Global.imageString(bitmap);
        }
        if(audioFilePath!=null && !audioFilePath.isEmpty()){
            File file = new File(audioFilePath);
            soundString = Global.fileToString(file);
        }

        DataController.doPostQuestion(query, imageString, soundString, this);
    }

    @Override
    public void asyncCallBackFunction(Object resultObject, String inputUrl) {

        if(inputUrl.equals(Global.FULL_URL)){

            Global.displayToast(activity, "Question Successfully posted.");
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result", "Success");
            setResult(Activity.RESULT_OK, returnIntent);
            activity.finish();
        }
    }

    Dialog dialog;
    MediaRecorder mediaRecorder;
    final String audioFilePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/myAudio.mp3";
    public void dialogRecording() {

        LayoutInflater inflater;
        dialog = new Dialog(activity);
        inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.dialog_audio_recorder, null);

        final Button start = (Button)layout.findViewById(R.id.start);
        final Button reset = (Button)layout.findViewById(R.id.reset);
        reset.setVisibility(View.GONE);
        final Button stop = (Button) layout.findViewById(R.id.stop);
        stop.setEnabled(false);
        final TextView status_text = (TextView) layout.findViewById(R.id.status_text);

        try {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mediaRecorder.setOutputFile(audioFilePath);
            mediaRecorder.prepare();
        }catch (Exception e){

        }

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (v.getId()){
                    case R.id.start:
                        mediaRecorder.start();
                        Global.displayToast(activity, "Recording started");
                        status_text.setText("Recording...");
                        start.setEnabled(false);
                        stop.setEnabled(true);
                        break;
                    case R.id.stop:
                        mediaRecorder.stop();
                        mediaRecorder.release();
                        dialog.dismiss();
                        Global.displayToast(activity, "Audio saved at location: " + audioFilePath);
                        status_text.setText("Recording Completed");
                        start.setEnabled(true);
                        stop.setEnabled(false);
                        showRecordClip();
                        break;
                    case R.id.reset:
                        Global.displayToast(activity, "Recording Re-stared");
                        mediaRecorder.reset();
                        status_text.setText("Start Recording");
                        start.setEnabled(false);
                        break;
                }
            }
        };
        start.setOnClickListener(onClickListener);
        stop.setOnClickListener(onClickListener);
        reset.setOnClickListener(onClickListener);

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layout);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void setUpPlayRecording() {

        playRecording = (FloatingActionButton) findViewById(R.id.playRecording);
        playRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
//                    MediaPlayer mediaPlayer = new MediaPlayer();
//                    mediaPlayer.setDataSource(audioFilePath);
//                    mediaPlayer.prepare();
//                    mediaPlayer.start();
                    dialogPlay();
                }catch (Exception e){

                }
            }
        });
    }

    MediaPlayer mediaPlayer;
    private void dialogPlay(){

        LayoutInflater inflater;
        dialog = new Dialog(activity);
        inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.dialog_audio_play, null);

        final Button stop = (Button) layout.findViewById(R.id.stop);

        View.OnClickListener onClickListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                switch (v.getId()){

                    case R.id.stop:
                        mediaPlayer.stop();
                        mediaPlayer.release();
                        dialog.dismiss();
                        break;
                }
            }
        };

        stop.setOnClickListener(onClickListener);

        try {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(audioFilePath);
            mediaPlayer.prepare();
            mediaPlayer.start();
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {

                    dialog.dismiss();
                }
            });
        }catch (IOException e){

        }

        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(layout);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.argb(0, 0, 0, 0)));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void setUpDeleteRecording() {

        deleteRecording = (FloatingActionButton) findViewById(R.id.deleteRecording);
        deleteRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    deleteRecordClip();
                }catch (Exception e){

                }
            }
        });
    }

    private void setUpReRecording() {

        reRecording = (FloatingActionButton) findViewById(R.id.rerecord);
        reRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    dialogRecording();
                }catch (Exception e){

                }
            }
        });
    }
}