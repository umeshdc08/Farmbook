package com.farmbook.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import com.farmbook.R;
import com.farmbook.activity.ActivityQuestionForm;
import com.farmbook.adapter.AdapterCrop;
import com.farmbook.adapter.AdapterQuestionAnswer;
import com.farmbook.model.Crop;
import com.farmbook.model.Farmer;
import com.farmbook.model.FarmerQuery;
import com.farmbook.utils.Global;

import java.util.Collections;
import java.util.List;


public class FragmentCropList extends android.support.v4.app.Fragment {

    View view;
    Activity activity;
    ListView  listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_crop_list, null);
        activity = (Activity) getActivity();

        setAllViews();
        return view;
    }

    private void setAllViews(){

        setupCropList();
    }

    private void setupCropList(){

        listView = (ListView) view.findViewById(R.id.crop_list);
        AdapterCrop cropAdapter = new AdapterCrop(activity, getCropList());
        listView.setAdapter(cropAdapter);
    }

    private List<Crop> getCropList(){

        return Global.cropList();
    }
}
