package com.farmbook.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.telecom.Call;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ca.commons.IBase.CallBackInterface;
import com.ca.commons.controller.DataController;
import com.farmbook.R;
import com.farmbook.activity.ActivityQuestionForm;
import com.farmbook.adapter.AdapterQuestionAnswer;
import com.farmbook.model.Farmer;
import com.farmbook.model.FarmerQuery;
import com.farmbook.model.QuestionAnswer;
import com.farmbook.utils.Global;
import com.gc.materialdesign.views.ButtonRectangle;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class FragmentTimeline extends android.support.v4.app.Fragment {

    View view;
    Activity activity;
    ButtonRectangle recording;
    FloatingActionButton writeQuestion;
    ListView  litView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_timeline, null);
        activity = (Activity) getActivity();

        setAllViews();
        return view;
    }

    private void setAllViews(){

        setupWriteQuestion();
        setupTimeLineList();
    }

    private void setupTimeLineList(){

        litView = (ListView) view.findViewById(R.id.timeline);
        AdapterQuestionAnswer questionAnswer = new AdapterQuestionAnswer(activity, questionAnswers());
        litView.setAdapter(questionAnswer);
    }

    private List<FarmerQuery> questionAnswers(){

        List<FarmerQuery> farmerQueries = Global.getFarmarQueries(activity);
        Collections.reverse(farmerQueries);
        return farmerQueries;
    }

    private void setupWriteQuestion(){

        writeQuestion = (FloatingActionButton) view.findViewById(R.id.action_post_question);
        writeQuestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, ActivityQuestionForm.class);
                activity.startActivityForResult(intent, Global.ACTIVITY_RESULT_QUESTION_POSTED);
            }
        });
    }

    private void saveSignupData(Farmer farmer){

        Global.setFarmerData(activity, farmer);
    }
}
